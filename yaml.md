<div dir="rtl" align="right">بنام خدا</div>
short note on Yaml file standard and syntax from [Page](https://learnxinyminutes.com/docs/yaml/)

# start and end
```yaml
---  # document start
...  # document end
```

# Comments
```yaml
# Comments in YAML look like this.
# YAML support single-line comments.
```

# Maps
Our root object will be a map which is equivalent to a dictionary, hash or object in other languages.

```yaml
key: value
another_key: Another value goes here.
a_number_value: 100
scientific_notation: 1e+12
hex_notation: 0x123  # evaluates to 291
octal_notation: 0123 # evaluates to 83
```
## Nesting Maps
uses indentation. 2 space indent is preferred (but not required).
```yaml
a_nested_map:
  key: value
  another_key: Another Value
  another_nested_map:
    hello: hello
```
above equivalent to:
```yaml
a_nested_map: { key: value, another_key: Another Value, another_nested_map: {hello: hello}}
```
- Maps don't have to have string keys.
```yaml
0.25: a float key
```
- Keys can also be complex, like multi-line objects
```yaml
? |
  This is a key
  that has multiple lines
: and this is its value
```
- Between sequences with the complex key syntax
  Some language parsers might complain
```yaml
? - Manchester United
  - Real Madrid
: [ 2001-01-01, 2002-02-02 ]
```
## Bool Maps
The number 1 will be interpreted as a number, not a boolean. 
- If you want it to be interpreted as a boolean, use true.
```yaml
boolean: true
null_value: null
key with spaces: value
```
- Yes and No (doesn't matter the case) will be evaluated to boolean 
```yaml
no: no            # evaluates to "false": false
yes: No           # evaluates to "true": false
not_enclosed: yes # evaluates to "not_enclosed": true
enclosed: "yes"   # evaluates to "enclosed": yes
```

## Strings
- Notice that strings don't need to be quoted. However, they can be.
```yaml
however: 'A string, enclosed in quotes.'
'Keys can be quoted too.': "Useful if you want to put a ':' in your key."
single quotes: 'have ''one'' escape pattern'
double quotes: "have many: \", \0, \t, \u263A, \x0d\x0a == \r\n, and more."
Superscript two: \u00B2 # UTF-8/16/32 characters need to be encoded
special_characters: "[ John ] & { Jane } - <Doe>" # Special characters must be enclosed in single or double quotes
```
Multiple-line strings can be written either as a 'literal block' (using |), or a 'folded block' (using '>').
1. **Literal block**: turn every newline within the string into a literal newline (\n).
```yaml
literal_block: |
  This entire block of text will be the value of the 'literal_block' key,
  with line breaks being preserved.

  The literal continues until de-dented, and the leading indentation is
  stripped.

      Any lines that are 'more-indented' keep the rest of their indentation -
      these lines will be indented by 4 spaces.
```
2. **Folded block**: removes newlines within the string.
```yaml
folded_style: >
  This entire block of text will be the value of 'folded_style', but this
  time, all newlines will be replaced with a single space.

  Blank lines, like above, are converted to a newline character.

      'More-indented' lines keep their newlines, too -
      this text will appear over two lines.
```
3. **Literal strip**: |- removes the trailing blank lines
```yaml
literal_strip: |-
  This entire block of text will be the value of the 'literal_block' key,
  with trailing blank line being stripped.
```
4. **Block strip**: >- removes the trailing blank lines
```yaml
block_strip: >-
  This entire block of text will be the value of 'folded_style', but this
  time, all newlines will be replaced with a single space and 
  trailing blank line being stripped.
```
5. **Literal keep**: |+ keeps trailing blank lines
```yaml
literal_keep: |+
  This entire block of text will be the value of the 'literal_block' key,
  with trailing blank line being kept.
```
6. ***Block keep**: >+ keeps trailing blank lines
```yaml
block_keep: >+
  This entire block of text will be the value of 'folded_style', but this
  time, all newlines will be replaced with a single space and 
  trailing blank line being kept.
```
## Sequences 
equivalent to lists or arrays, look like this
```yaml
a_sequence:
  - Item 1
  - Item 2
  - 0.5  # sequences can contain disparate types.
  - Item 4
  - key: value
    another_key: another_value
  - - This is a sequence
    - inside another sequence
  - - - Nested sequence indicators
      - can be collapsed
```
equivalent to:
```yaml
a_sequence: [Item 1, Item 2, 0.5, Item 4, {key: value}, {another_key: another_value}, [this is a squence, inside another sequence],
              [[Nested sequence indicators, can cooalpsed]] ]
```

- Since YAML is a superset of JSON, you can also write JSON-style maps and
```yaml
json_map: { "key": "value" }
json_seq: [ 3, 2, 1, "takeoff" ]
and quotes are optional: { key: [ 3, 2, 1, takeoff ] }
```
## let me call Reference
YAML also has a handy feature called 'anchors', which let you easily duplicate content across your document.
### Anchor
Anchors identified by **&** character which define the value.Aliases identified by **\*** character which acts as "see above" command.
Both of these keys will have the same value:
```yaml
anchored_content: &anchor_name This string will appear as the value of two keys.
other_anchor: *anchor_name
```
- Anchors can be used to duplicate/inherit properties
```yaml
base: &base
  name: Everyone has same name
```
### Merge Key
The regexp **<<** is called 'Merge Key Language-Independent Type'. It is used to indicate that all the keys of one or more specified maps should be inserted into the current map.
- NOTE: If key already exists alias will not be merged
```yaml
foo:
  <<: *base # doesn't merge the anchor
  age: 10
  name: John
bar:
  <<: *base # base anchor will be merged
  age: 20
```
> foo and bar would also have name: Everyone has same name

## Tags
YAML also has tags, which you can use to explicitly declare types.
Syntax: !![typeName] [value]
```yaml
explicit_boolean: !!bool true
explicit_integer: !!int 42
explicit_float: !!float -42.24
explicit_string: !!str 0.5
explicit_datetime: !!timestamp 2022-11-17 12:34:56.78 +9
explicit_null: !!null null
```
-  Some parsers implement language specific tags, like this one for Python's complex number type.
```
python_complex_number: !!python/complex 1+2j
```
- We can also use yaml complex keys with language specific tags
```yaml
? !!python/tuple [ 5, 7 ]
: Fifty Seven
# Would be {(5, 7): 'Fifty Seven'} in Python
```
## data types
Strings and numbers aren't the only scalars that YAML can understand. ISO-formatted date and datetime literals are also parsed.
```yaml
datetime_canonical: 2001-12-15T02:59:43.1Z
datetime_space_seperated_with_time_zone: 2001-12-14 21:59:43.10 -5
date_implicit: 2002-12-14
date_explicit: !!timestamp 2002-12-14
```
- The !!binary tag indicates that a string is actually a base64-encoded, representation of a binary blob.
```yaml
gif_file: !!binary |
  R0lGODlhDAAMAIQAAP//9/X17unp5WZmZgAAAOfn515eXvPz7Y6OjuDg4J+fn5
  OTk6enp56enmlpaWNjY6Ojo4SEhP/++f/++f/++f/++f/++f/++f/++f/++f/+
  +f/++f/++f/++f/++f/++SH+Dk1hZGUgd2l0aCBHSU1QACwAAAAADAAMAAAFLC
  AgjoEwnuNAFOhpEMTRiggcz4BNJHrv/zCFcLiwMWYNG84BwwEeECcgggoBADs=
```
### Set type
YAML also has a set type, which looks like this:
```yaml
set:
  ? item1
  ? item2
  ? item3
or: { item1, item2, item3 }
```
- Sets are just maps with null values; the above is equivalent to:
```yaml
set2:
  item1: null
  item2: null
  item3: null
```
