<div dir="rtl" align="right">بنام خدا</div>

- [Chapter One](#chapter-one)
  - [Affinity](#affinity)
  - [Classification](#classification)
  - 
  
- [Chapter Two](#chapter-Two)

- [Chapter Three](#chapter-Three)

- [Chapter Four](#chapter-Four)

- [Chapter Five](#chapter-Five)

- [Chapter Six](#chapter-Six)

- [Chapter Seven](#chapter-Seven)

- [Chapter Eight](#chapter-Eight)

- [Chapter Nine](#chapter-Nine)

- [Chapter Ten](#chapter-Ten)

- [Chapter Eleven](#chapter-Eleven)

- [Chapter Towelve](#chapter-Towelve)

- [Chapter Thirteen](#chapter-Thirteen)


[top](#top)
# Chapter One
## Affinity
or Product recommendations:
in this algorithm we should:
  * called main feature-> **Premise** and features who bought with it **Conclusion**.
  1. for each _Premise_ record which features bought and keep total number occurenced of _Premise_ and each _Conclusion_.
  2. find frequency of each _Conclusion_ for each _Premise_
  3. Ranking them
  4. make decision

Codes:
```python
  import numpy as Np
  from collection import defaultdict
  from operator import itemgetter
  from matplotlib import pyplot as Plt
  
  DataSet= Np.loadtxt("affinity_dataset.txt")
  FeatureName=["Bread","Milk","Cheease","Apple","Bnannas"]
  SampleNo,FeatureNo=DataSet.shape
  valid_rules=invalid_rules=OccunreceNo=defaultdict(int)
  Confidence=defaultdict(float)
  for Sample in DataSet:
    for Premise in range(FeatureNo):
      if Sample[Premise]==0: continue <----------------------------------------- Customer Did not buy this Commodity
      OccurenceNo[Premise] +=1  <----------------------------------------------- One time buying occurred 
      for Conclusion in range(FeatureNo): <------------------------------------- Iterate over all features
        if Premise==Conclusion: contu=inue <------------------------------------ Both are the same Commodity
        if Sample[Conclusion]: valid_rules[(Premise,Conclusion)] +=1 <---------- Record Number of bought both
        else invalid_rules[(Premise,Conclusion)] +=1
  for Premise,Conclusion in valid_rules.keys():
    Confidence[(Premise,Conclusion)]= valid_rules[(Premise,Conclusion)]/OccurenceNo[Premise] <--- Percent Occurances
  # Confidence Object is a defaultdict and like this: defaultdict(<class 'float'>, {(0, 1): 0.4642857142857143, ...
  ConfidenceSort=sorted(Confidence.items(),key=itemgetter(1),reverse=True) <---- items() gives a list of data, itemgetter(1) allow to sort on Value
  # ConfidenceSort object is a list and like this: [((3, 4), 0.627906976744186), 
  for Keys,Value in ConfidenceSort:
    Premise,Confidence=Keys
    print("{0:.2f} percent of buying {1} when someone bought {2}".format(Value,Premise,Confidence)
  Plt.plot(Item[1] for Item in ConfidenceSort)
```

[top](#top)
## Classification
is most usefull solution in datamining. it is _answering_ to **binary Question** Yes|No.
### OneR Algorithm
in this section we used [OneR](http://www.saedsayad.com/oner.htm) algorithm.
in short explanation: move through all features and count each of them for each values(they should be 1,0 only) and for each value of it, 
call count of Rows that _has the other value_ as **Error**.
finnaly find **Feature** with **Smallect** _Errors_

* if features are _continues_ data we sould **discrete** them(discretization)
* 

```python
  from sklearn.datasets import load_iris
  from sklearn.cross_validation import train_test_split as Tts
  from operator import itemgetter
  from collections import defaultdict
  import numpy as Np
  
  Iris=load_iris
  ContinuesData,Target=Iris.data,Iris.target <--------------------------- these features are continues
  Features_Mean=ContinuesData.mean(axis=0)
  Dt=Np.array(ContinuesData>=Features_Mean,dtype='int')
  DataNo,FeatureNo=Dt.shape
  TrainDt,TestDt,TrainTgt,TestTgt=Tts(Dt,Target,random_state=14) <------- randon_state Option guarantees same result in each run. :-)
  Model=defaultdict(dict)
  Result=Err=defaultdict(int)
  Times=0
  for Feature in FeatureNo:
    for Value in set(Dt[:,Feature]):
      for Row,Trgt in zip(Dt[:,Feature],Target):
        if Row==Value : Result[Trgt]+=1
    for Current_Y in Result.keys():
      Err[Feature]+=sum([Y_Value for Y_Name,Y_Value in Result.items() if Y_Name != Current_Y])
        
  
```
[top](#top)
# Chapter Two

[top](#top)

# Chapter Three

[top](#top)

# Chapter Four

[top](#top)

# Chapter Five

[top](#top)

# Chapter Six

[top](#top)

# Chapter Seven

[top](#top)

# Chapter Eight

[top](#top)

# Chapter Nine

[top](#top)

# Chapter Ten

[top](#top)

# Chapter Eleven

[top](#top)

# Chapter Towelve

[top](#top)

# Chapter Thirteen

[top](#top)















