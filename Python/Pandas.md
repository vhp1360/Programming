<h1 align="right" dir="rtl">بنام خدا</h1>

- [Csv](#Csv)
  - [Csv to Json](#Csv_to_Json)



# Csv
## Csv to Json
```python
    import pandas as pd
    load_path="..."
    save_path="..."
    pd.read_csv(load_path,encoding="utf-8").to_json(save_path,force_ascii=False)
```
### Snippet code for directory
```python
    import os
    import pandas as pd
    path="..."
    for filename in os.path.join(path,"*.csv"):
        f_name,f_ex = os.path.basename(filename).split('.')
        pd.read_csv(filename,encoding="utf-8").to_json(os.path.join(os.path.dirname(filename)+"/"+f_name+".json",force_ascii=False,orient="records")
```
