<div align=right dir=rtl>بنام خدا</div>

- [File Structure](#file-structure)
- [Type](#type)
    - [Punctuation in identifiers](#punctuation-in-identifiers)
        - [Some Globale Variables](#some-globale-variables)
    - [String literal](#string-literal)
        - [print Unicode](#print-unicode)
        - [One Character](#one-character)
        - [String Operations](#string-operations)
            - [Substitution](#substitution)
            - [Iteration](#iteration)
                - [Custom Iteration](#custom-iteration)
            - [Regular Experssion](#regular-experssion)
            - [Matches](#matches)
    - [Integer](#integer)
    - [Array](#array)
    - [Hashes](#hashes)
    - [Time](#time)
    - [Ranges](#ranges)
    - [Symbols](#symbols)
- [Flow Controls and Conditions](#flow-controls-and-conditions)
  - [if](#if)
  - [unless](#unless)
  - [case](#case)
- [Loops](#loops)
  - [for Loops](#for-loops)
  - [while Loops](#while-loops)
  - [until Loops](#for-loops)
  - [begin,end Loops](#for-loops)
  - [loop](#loop)
  - [Altering Control Flow](#altering-control-flow)
- [Experssions](#experssions)
    - [Constant](#constant)
    - [Assignment](#assignment)
    - [Multiple Assigning](#multiple-assigning)
- [CodeBlocks,Functions,Procedures,Lambdas](#odeblocksfunctionsprocedureslambdas)
  - [Code Blocks](#code-blocks)
  - [Functions](#functions)
  - [Procedures](#procedures)
  - [Lambda](#lambda)
- [Class](#class)   
  - [Inheritance](#inheritance)
  - [Nested Class](#nested-class)
  - [Class Example](#class-example)
  - [Object](#object)
    - [Object Ordering](#object-ordering)
    - [Conversions](#conversions)
    - [Coercion](#coercion)
    - [Boolean Conversion](#boolean-conversion)
  - [Copying Object](#copying-object)
  - [Marshaling Objects](#marshaling-objects)
  - [Object Frozen](#object-frozen)
  - [Objects Safe Levels](#objects-safe-levels)
- [Namespaces, Modules and Mix-Ins](#namespaces-modules-and-mix-ins)
- [Useful Class,Module,Objects,etc](#useful-classmoduleobjectsetc)
  - [Structure](#structure)
  - [Run OS Command](#run-os-command)
- [Error Handeling,Debugging,Testing](#error-handelingdebuggingtesting)
    - [Error Handling](#error-handling)
      - [Raise Exception](#raise-exception)
      - [Rescue Exception](#rescue-exception)
        - [Naming Error in Rescue](#naming-error-in-rescue)
      - [Catch Throw](#catch-throw)
    - [Debugging](#debugging)
        - 
    - [Testing](#testing)
        - 
- [Documantation](#documentation)
    - 
- [IO](#io)
    - 
- 
[top](#top)

# File Structure
the rubys file structure would like below:
```rb
    #!/usr/bin/ruby -w         shebang comment 
    # coding: utf-8            coding comment
    require 'socket'           load networking library
    ...                        program code goes here
    __END__                    mark end of code
    ...                        program data goes here
```
# Type

*IMPORTANT*: Two identifiers are the same only if they are represented by the same sequence of bytes.
             Some character sets, such as Unicode, have more than one codepoint that represents the same character. 
             No Unicode normalization is performed in Ruby, and two distinct codepoints are treated as distinct characters, 
             even if they have the same meaning or are represented by the same font glyph.

## Punctuation in identifiers
Name | Begins With	Variable Scope
---|---
$ |	A global variable
@ |	An instance variable
@@ | A class variable
? | word end it,good for return boolean value
! | word ent with it, means change itself, not make copy of it.
= | ...
[a-z] or _ |	A local variable
[A-Z] |	A constant

###   Some Globale Variables

Variable Name |	Variable Value
---|---
[email protected] |	The location of latest error
$_ | The string last read by gets
$. | The line number last read by interpreter
$& | The string last matched by regexp
$~ | The last regexp match, as an array of subexpressions
$n | The nth subexpression in the last match (same as $~[n])
$= | The case-insensitivity flag
$/ | The input record separator
$\ | The output record separator
$0 | The name of the ruby script file currently executing
$* | The command line arguments used to invoke the script
$$ | The Ruby interpreter's process ID
$? | The exit status of last executed child process
$? | The exit status of last executed child process

[top](#top)
## String literal
**Strings** in _Ruby_ is a mutable Object in spite of other languages.it shows in some ways:
1. _Single quote_: in this way, _BackSlash_ sign only scape itself and singleqoute sing.
2. _Double qoute_: scape all. you could pass experssion in `#{Experssion}` way and variable like `#$VarName`.
    - _\#_ sign in _Double Qoute_ no need Backslash to scape => `puts "word # ...."`
    - _\#\{_ in _Double Qoute_ consider as one char and only need one Backslash to scape: `puts "word \{# ..."`
3. there are two other function `printf` and `sprintf` and also two other string prints:
```rb
  "pi is about %.4f" % Math::PI
  "%s: %f" % ["pi", Math::PI]
```
    - Use ri to learn more: ri Kernel.sprintf
4. _String Delimiter_: you could start string with **%q** (simulate _Single Qoute_) and **%Q** (simulate _Double Qoute_) followed by one _delimiter_ **Sign** followed by String with any characters exclude _delimiter_ one unless _scape it_ with _backSlash sign_ followed by **Matching** _delimiter_ sign as last char.
    - Match characters for ({[< are ]}).
    - any Internal Pairs characters are _scape_ automatically.
    - _%Q_ is same with _%_ sign.
```rb
  puts %q(any chars like `~'" ...)
  puts %q! like above and \! will scape exclemation sign!
  puts %q[ this type of string scape any internal delimiter [like this] woud print this sign]
  puts %q# all of them the same result with %Q operand#
```
5. **Shell** _Here Document_ is here to: 
```rb
  Doc = <<EOF1+ <<EOF2 + ...
  some lines
  could be here
  ...
  EOF1
  and some others
  would be here
  for next Here Doc
  EOF2
  ...
```
    
Escape sequence | Meaning
---|---
\\x | A backslash before any character x is equivalent to the character x by itself, unless x is a line terminator or one of the special characters abcefnrstuvxCM01234567. This syntax is useful to escape the special meaning of the \, #, and " characters.
\\a | The BEL character (ASCII code 7). Rings the console bell. Equivalent to \C-g or \007.
\\b | The Backspace character (ASCII code 8). Equivalent to \C-h or \010.
\\e | The ESC character (ASCII code 27). Equivalent to \033.
\\f | The Form Feed character (ASCII code 12). Equivalent to \C-l and \014.
\\n | The Newline character (ASCII code 10). Equivalent to \C-j and \012.
\\r | The Carriage Return character (ASCII code 13). Equivalent to \C-m and \015.
\\s | The Space character (ASCII code 32).
\\t | The TAB character (ASCII code 9). Equivalent to \C-i and \011.
\\unnnn | The Unicode codepoint nnnn, where each n is one hexadecimal digit. Leading zeros may not be dropped;all four digits are required in this form of the \u escape. Supported in Ruby 1.9 and later.
\\u{hexdigits} | The Unicode codepoint(s) specified by hexdigits. See the description of this escape in the main text.Ruby 1.9 and later.
\\v | The vertical tab character (ASCII code 11). Equivalent to \C-k and \013.
\\nnn | The byte nnn, where nnn is three octal digits between 000 and 377.
\\nn | Same as \0nn, where nn is two octal digits between 00 and 77.
\\n | Same as \00n, where n is an octal digit between 0 and 7.
\\xnn | The byte nn, where nn is two hexadecimal digits between 00 and FF. (Both lowercase and uppercase letters are allowed as hexadecimal digits.)
\\xn | Same as \x0n, where n is a hexadecimal digit between 0 and F (or f).
\\cx | Shorthand for \C-x.
\\C-x | The character whose character code is formed by zeroing the sixth and seventh bits of x, retaining the high-order bit and the five low bits. x can be any character, but this sequence is usually used to represent control characters Control-A through Control-Z (ASCII codes 1 through 26). Because of the layout of the ASCII table, you can use either lowercase or uppercase letters for x. Note that \cx is shorthand. x can be any single character or an escape other than \C \u, \x, or \nnn.
\\M-x | The character whose character code is formed by setting the high bit of the code of x. This is used to represent “meta” characters, which are not technically part of the ASCII character set. x can be any single character or an escape other than \M \u, \x, or \nnn. \M can be combined with \C as in \M-\C-A.
\\eol | A backslash before a line terminator escapes the terminator. Neither the backslash nor the terminator appear in the string.

[top](#top)
### print Unicode
- if you would print **Unicode** char, there are two ways in _Ruby_.
    1. **\\unnnn** like \u00D7 or \u20ac ==> "x€"
    2. **\u{as Much}** like \u{A5} or \u{3C0} ==> "¥π"
        - \u00A5 ~ \u{A5}
        - multiple char allow in this way: "\u{20AC A3 A5}" ==> "€£¥"
        - space is _20_: "\u{20AC 20 A3 20 A5}" ==> "€ £ ¥"
 

#### One Character
there is a special way in ruby to show **one Char**, instead of _Qoutes_ you could use _Question Mark_.it could contain any _Scape Chars_.
```rb
  q= ?A             # q value is 'A'
  q= ?€             # now q is € that has 20AC unicode codepoint.
  ?€ == \u20AC      # true
  ?€ == "\u20AC"    # true
  q = ?\t
  q = 'A'
  q << ?B
  q << 67           # now q is 'ABC'
```
[top](#top)
## String Operations  
most _String Operation_ done in _curly Brackets_.
```rb
  S="This is a sample String"
  S[0]                      ==> 'T'
  S[S.length-1]             ==> 'g'
  S[S.length]               ==> nil
  S[-3]                     ==> 'i'
  S[5,8]                    ==> 'is a sam'
  S[-1]= ?G                 ==> 'This is a sample StrinG'
  S[-8,5]                   ==> 'e Str'
  S[8,-1]                   ==> nil
  S[5..8]                   ==> 'is a'
  S[-8,-5]                  ==> 'e St'
  
```
- **All above example** use _integer_ as Index.we could use _Letters_ as Index for **Iteration**, consider below example:
```rb
  while (S['i'])
    S['i']= 'I'
  end                       ==> 'ThIs Is a sample StrInG'
```
  
### Substitution
```rb
  puts "strings.....".sub("SomeThings","SomeThingsElse")
```

[top](#top)
### Iteration
**Iterator** is main part for looping.it used **yield** statement to control flow of iterating.there are some example of Iterate function:
```rb
  3.times { |a| puts a}                                                             # NUmeric Iterator
  [1,2,3,4].each {|a| puts a}                                                       # for Enumerable Objects
  (1..5).each {|a| puts a}                                                          # for Enumerable Objects
  File.open(FileName) {|f| f.each_with_index {|l,i| puts '#{i}:#{l}'}}              # for Enumerable Objects
  array1.map {|a| puts a*a} ~ array1.collect                                        # Numeric Iterator
  factorial=1
  2.upto(n) {|a| factorial*=a}                                                      # Numeric Iterator
  10.downto(0) {...}                                                                # Numeric Iterator
  2.1.step(Math::PI,0.01) {|a| puts a}                                              # Numeric Iterator
  (0..10).select {|a| a%2==0} !~ (0..10).reject ...                                 # NUmeric Iterator
  (1..10).inject {|Sum,a| Sum+a}                                                    # NUmeric Iterator
  [3,6,2,8,5].inject {|Max,a| Max>a?Max:a}                                          # NUmeric Iterator
```
1. There are Some ways to _Iterate_ over Strings:
    1. **each_byte**: _Iterate_ over each byte of String
    2. **each_char**: _Iterate_ over each char of String
    3. 
    ```rb
      "String...".each_byte {|q| puts q}
      "String...".each_char ...
      "Strings.....".scan(/../) do |q| puts q  //puts 2 characters each time
    ```
2. **to_enum** and **enum_for** are synonym defined in _Object_ level and useful to pass _Object_ which not Iterable by default.
```rb
  s="this is a string"
  s.to_enum(:each_char).map {|q| puts q.succ}
```
actually we could write above code like below
```rb
  s.each_char {|q| puts q.succ} 
  # or
  s.chars.map ...
```
3. You can duplicate _Enumaration_ behavior in your own iterator methods by returning **self.to_enum** when no block is supplied:
```rb
  def twice
    if block_given?
      yield
      yield
    else
      self.to_enum(:twice)
    end
  end
```
4. **External Iterate** means _Client_ control the _Iterating_. this achieved by **next** _Operand_:
```rb
  enum1=10.downto(1)
  print enum1.next while true
```
**Next** raise _Exception_ when there is no _Next Item_
```rb
  enum1 = 10.downto(1)
  begin
  	print enum1.next while true
  rescue StopIteration
  	puts "... Off"
  
  end
```
5. if an object has _External_ iteration we could simply define _Internal_ iteration:
```rb
  module Itarator
    include Enumerable
    def each
      loop { yield self.next}
    end
  end
```
6. I could not understand this:
```rb
  def bundle(*enumerables)
  	enumerators = enumerables.map {|e| e.to_enum }
  	loop { yield enumerators.map {|e| e.next} }
  end
  bundle(1..4,'a'..'d') {|q| print q}                    #=> [1,'a'] [2,'b'] [3,'c'] [4,'d']
```
7. **Iteration** iterate over current **Object**. we could define a new _method_ to prevent this:
```rb
  module Enumerable
    def each_in_snapshot &block
      snapshot=self.dup
      snapshot.each &block
    end
  end
```
8. **yield** return more than _one_ value:
```rb
  def two; yield 1,2; end                       # An iterator that yields two values
  two {|x| p x }                                # Ruby 1.8: warns and prints [1,2],
  two {|x| p x }                                # Ruby 1.9: prints 1, no warning
  two {|*x| p x }                               # Either version: prints [1,2]; no warning
  two {|x,| p x }                               # Either version: prints 1; no warning
  def five; yield 1,2,3,4,5; end
  five {|head,body,tail| print q,a,z}           # => 123
  five {|head,*body,tail| print q,a,z}          # => 1[2,3,4]5
```
9. [Altering Control Flow](#altering-control-flow) work here.
10. 


##### Custom Iteration
we could define custom iterating with _yield_ statement.
```rb
  def MyFunc(q,a,z)
    0.upto(q) do |w|
      yield a*w+z if block_given?           # it is useful to prevent raising error
    end
  end
  MyFunc(10,2,5) {|a| puts a}
```


[top](#top)
### Regular Experssion
- Fisrt Table:

Character | Meaning
---|---
^  | Anchor for the beginning of a line
$  | Anchor for the end of a line
\A | Anchor for the start of a string
\z | Anchor for the end of a string
.  | Any character
\w | Any letter, digit, or underscore
\W | Anything that \w doesn’t match
\d | Any digit
\D | Anything that \d doesn’t match (non-digits)
\s | Whitespace (spaces, tabs, newlines, and so on)
\S | Non-whitespace (any visible character)

- Second Table:

Modifier | Description
---|---
\*     | Match zero or more occurrences of the preceding character, and match as many as possible.
\+     | Match one or more occurrences of the preceding character, and match as many as possible.
\?     | Match either one or none of the preceding character.
\*\?   | Match zero or more occurrences of the preceding character, and match as few as possible.
\+\?   | Match one or more occurrences of the preceding character, and match as few as possible.
\{x\}  | Match x occurrences of the preceding character.
\{x,y\}| Match at least x occurrences and at most y occurrences.


[top](#top)
### Matches
```rb
  puts "there is not any digit in this sentence" unless "sample Sentence" =~ /[0-9]/
  # another way as always
  puts "there is not any digit in this sentence" unless "sample Sentence".match(/[0-9]/)
```
  - be care like below:

  ```rb
    x = "This is a test".match(/(\w+) (\w+) (\w+)/)
    => #<MatchData "This is a" 1:"This" 2:"is" 3:"a">
    x.length
    => 4
    x = "This is a test".match(/\w+ \w+ \w+/)
    => #<MatchData "This is a">
    x.length
    => 1
  ```
[top](#top)
### Char Encoding
How many _bytes_ assign to each _Character_? this is the main issue on **Encoding**.if two String have different _char Encoding_ they wouldn't be _concatinated_.
- to change text _encoding_ while writting you would use `# _*_ coding: EncodingName _*_` above text.
- to immediately change String _encoding_ should use `force_encoding("EncodingName")`.
- if you use `force_encding` on uncompatible String, _it would not generate error_ but not used. to check does new _encoding_ valid `va;id_encoding?`
- to find current String _encoding_ use `ecoding` function.
- to check whether two string have same _encoding_, we could use **Encoding.compatible?** function.
- `encode` command could accept two _Param_: `encode(to,from)` or you could use it with `force_encoding` like `force_encoding(from).encode(to)`

##### Encoding
there is another class **Encoding** that have some events like `::EncodingName` , `.default_external` , `.list` , `.find` , `.locale_charmap`

  
[top](#top)
## Integer
- any sequence of digits literal appears an integer.
- if length that is short consider as _**FixNum**_ otherwise would be _**BigNum**_.
- an Integer which start with _0_ number would has different base num than 10:
    - _**0x** or **0X**_ represent as **HexaDesimal**.
    - _**0b** or **0B**_ represent as **Binary**.
    - _alone **0**_ at the first means **Octal** numeric.
- to make an Integer readable format, we could use _Underscore_ sign: 1000000 is the same as 1\_000\_000
- _Minus_ sign used to represent a negative Integer
- **Division** and **Module** result: if both are Int => result in Int, otherwise Float:
```rb
  x = 5/2     # result is 2
  y = 5.0/2   # result is 2.5  
  z = 5/2.0   # result is 2.5  
  x = 5%2     # result is 1
  x = 1.5%0.4 # result is 0.3
```
---
- _**Division on Negative Int**_: 
  - \-5/3 => \-2 , _the ruby round result to **Lower bound**_
  - \-a/b ~ a/\-b !~ \-(a/b)
---


[top](#top)
## Array
- is [...] and can accept each type in one array. some sample of _Array_ creation:
```rb
  A=[1,2,5,"aaa",[3,4],q*a,-10..0,0..8,14,('a'..'d').to_a]
  A << ['sss','rrr'] << 'zzzz'
  B=%w[Word1,Word2,Word3]
  C=%w| ( { \s|
  Empty     = Array.new()
  Nils      = Array.new(5)
  Identity  = Array.new(8){|q| ++q}         #==> {0..7}
  Copy      = Array.new(Identity)
  Unit      = Array.new(10,1)               #==> {1,1,.....,1}
  A[-A.size]                                #==> 1
  A[A.size-1]                               #==> 14
  A[6]                                      #==> -10..0
  A[9][2]                                   #==> 'c'
  A[0...-1]                                 #==> Would return all except last one
  A[0..-1]                                  #==> Would return all include last one
```
  
- string to array:

```rb
  p "Strings.....".split(/\./){.inspect}   # _inspect_ is optional
```
[top](#top)
- deal with array:

```rb
  [...].each {|elem| ....}
  [...].collect{|elem| ....}                                                    #result is a array
  %w{word1 word2 word3 ...}.select{|word| !%w{elem1 elem2 ...}.include?(word)}  # return an array that not include in clause
  %w{word1 word2 word3 ...}.reject{|word| !%w{elem1 elem2 ...}.include?(word)}  # return an array that include in clause
  String.split(/.../)                                                           #return an array, default seperator is Space
  Array.join('...')                                                             # return a string, default joinner is Space
  A[No1,Count1]= nil                                                            # replace Items from No1 til Count1 with Nil
  A[No1,Count1]= Any New Replacement but Only Array Allow                       # replace Items from No1 til Count1 with Replacement Array
  
  
```
* _Other_ useful array **Operators**
    - **Clear**
    - **compact!**
    - **delete_if**
    - **each_index**
    - **empty?**
    - **fill**
    - **flatten!**
    - **include**
    - **index & rindex**
    - **join**
    - **pop & push**
    - **reverse & reverse_each**
    - **shift & unshift**
    - **sort & sort!**
    - **uniqu!**

[top](#top)
## Hashes
**Hash** is a Data Structure and known as **Map** or **associative Arrays** also.
```rb
  Num = Hash.new
  Num['One']=1
  Num['Tow']=2
  Num['Three']= Num['One']+Num['Two']
```
Another way to define _Hash_:
```rb
  people = {'Frist' => {'name' => 'Mohammad',:age => 63,:gender => 'male',
                            'favorite painters' => ['Monet', 'Constable', 'Da Vinci']},
            :Second => {:name => 'Ali','age' => 55,'gender' => 'female'}}
  puts people['Frist'][:age]
  puts people[:Second]['gender']
  p people[:Second]
```
- And in some version `Hash1={:one,1,:two,2}` define a **Hash** but hard to read.


[top](#top)
## Time
- time in Ruby store ms from 1/1/97, it means you could do calculation on it.

```rb
  Time.now + 10 # be care _10_ here means __10 Second not milisecond__
```
  - some developer extend ___Fixnum___ class to below:

  ```rb
    class Fixnum
      def minutes
        self*60
      end
      def hours
        self * 60 * 60
      end  
      def days
        self * 60 * 60 * 24
      end  
    end
  ```
  then we could use command like:

   ```rb
     Time.now + 19.hours
   ```
  - _Time.local_ , _Time.gm_ , _Time.utc_ are local time, greendwich and Universal time respectively:

  ```rb
    Time.local(yAER,mONTH,dAY,hOUR,mINUTE,sECOND,mILISECOND) # all are numbers
  ```
  - Find Time element from number:

  ```rb
    Time.at(1431431418).year
    # => 2015
  ```
- Time Object Methods Used to Access Date/Time Attributes:

Method | Returns
---|---
hour | A number representing the hour in 24-hour format ( 21 for 9 p.m., for example).
min | The number of minutes past the hour.
sec | The number of seconds past the minute.
usec | The number of microseconds past the second (there are 1,000,000 microseconds per second).
day | The number of the day in the month.
mday | Synonym for the day method, considered to be “month” day.
wday | The number of the day in terms of the week (Sunday is 0 , Saturday is 6 ).
yday | The number of the day in terms of the year.
month | The number of the month of the date ( 11 for November, for example).
year | The year associated with the date.
zone | Returns the name of the time zone associated with the time.
utc? | Returns true or false depending on if the time/date is in the UTC/GMT time zone or not.
gmt? | Synonym for the utc ? method for those who prefer to use the term GMT.
  for example:
  ```rb
    irb(main):027:0> Time.at(1431431418).wday
    # => 2
  ```
[top](#top)
## Ranges
its simple:
```rb
  ('a'..'z')
```
  - it has some methodes:
    1. convert to aray: `('a'..'z').to_a`
    2. `each` methode like as always.
    3. `step(No)` like above but iterate with _No_ Step
    4. member test: `('a'..'z').member?('S')`. however, there are two other methods _include?_ that the same as _member_ but **faster** 
       and _cover?_ that more fast and use continus mode for example `('A'..'Z').cover? 'BC'` return _**True**_.

## Symbols
in a simple explanation: like string, but same _Symbole_ use one place of memory. _Symbols_ and _strings_ are convertable to each.
the Symbol is useful for reference object for example instead of "Name1" and "Name2" for reference, using :Name1 and :Name2
All Symbols save in _Symbole Table_ and `puts Symbol.all_symbols` list all existing _Ruby Symbole_ in runtime.
```rb
  sYmbole= :Name
  str="String"
  sym= str.to_sym
  sym=str.intern
  str=sym.to_s
  str=sym.id2name
  sym1.__id__
  sym1.object_id
```
- it has `to_s` methode to convert _Symbole_ to _String_ and also `size`.

# Flow Controls and Conditions
### if
```rb
  if Condition Clause
    result
  elsif ...
    ...
  else
    ...
  end
  Variable= Condition Clause? TrueResult:FalseResult
```
### unless
it is opposite of **if**:
```rb
  unless Condition                      # as Statement
    Code
  else
    Code
  end
  
  Code uless Condition                  # as Modifier
  Var1=uless Condition then Val1        # in One Line
```

[top](#top)
### case
```rb
  case Param 
    when FirstCondition
        Variable=Result1
    when SecondCondition then Variable=Result2
    when 3thCondition; Variable=Result3
    ...
    else
    ...
    end
  # another Way
  Variable= case Param
            when FirstCondition
                Result1
            ...
            end
  puts case experssion
        when 'a'..'z' then ...
        when 1..10 then ...
        ...
        else
        end
```
1. **Class** class defines _===_ that useful to find _right hand_ class definition:
```rb
a='Test'
puts case
	      when Integer===a then "'a' is a Integer"
	      when Fixnum===a then "'a' is a Fixnum"
	      when String===a then "'a' is a String"
	      else "None"
      end
```
    
[top](#top)

# Loops
### for Loops
```rb
 hash1={:key1=>Val1,:key2=>Val2,:key3=>Val3}
 for key1,val1 in hash1 do
   puts "{Key=#{key1} => Value=#{vla1}}"
 end  
```

### while Loops
+ becare when **while** or **until** used as modifier, we should used them in single line,therefore _first_ condition test and so on.

[top](#top)
### until Loops
it is opposite of **while**
```rb
  a=[1,2,3,4]
  puts a.pop until a.empty?
```
### begin,end Loops
```rb
  x = 10
  begin
    puts x
    x = x - 1
  end until x == 0
```
### loop

### Altering Control Flow
there are a few _statement_ those alter the control of flow:
  1. **return** causes a _method_ to exit and return _value_.
    1. if _return_ followed by list of experssion should seperate with _comma_ and result would be _Array_.
    2. most of **flow** does not need _return_ and last value of _flow_ would be consider as _return value_.
  2. **break** exit a loop.
  3. **next** causes a loop to _skip_ rest of flow.
  4. **redo** restart a loop from top of block.
  ```rb
    q=0;while q<3 { print q; q+=1;redo if q==3}         #=> 0123
  ```
  5. **retry** _reevaluating_ and _restart_ complete.
  6. **throw/catch** find it [here](#catch-throw)
    
[top](#top)


[top](#top)

# Experssions
there is no clear distinction way in **Ruby** to distingush between _Experssions_(such as literal,value refrences,...) and 
_Statemanet_(contains some experssions combines with operators) and all _Experssion_ in **Ruby** would be( and have) **Evaluated** rather than **Executed**.
some main primary _keyword literals_ are:
```rb
  nil                   #==> nil value of NilClass
  true,false            #==> evaluates as TrueClass and ...
  self                  #==> evaluates to the current Object
  __FILE__,__LINE__     #==> good for debuging and return current file name and line number
  __ENCODING            #==> return Encoding of current file
```
---
What happened if you used a variable before definition?:
1. if it is a _Class Variable_(name start with double \@): ruby return a **NameError** exception.
2. if it is a _Instance Variable_(name start with \@): simply return **nil**.
3. if it is a _Global Variable_(name start with double \$): like above.
4. if it is a _Local Variable_: it would be more complecate of before Items.if _there is variable with that name_ ruby return value, 
    _if not_ ruby try to find _method_ with that name and if _could not_ raise **NameError** exception.

### Constant
_Constants_ are like _variables_. **::** would be references sign for _Constant_ and _Modules_.
```rb
  Conversions::CM_PER_INCH 
  modules[0]::NAM
  Conversions::Area::HECTARES_PER_ACRE
  ::ARGV
```
---
> the last one call Global Constant ARGV._Actually there is not a **"global scope"**  for Constant_.like global function,**global constants are defined 
>  and looked up within the _object class_.
---
[top](#top)

###### how does **puts** method invoked?
there are some _methodes_ in **Ruby Kernel** those are global functions and they are defined in **Object** class and when we invoke `puts` its actually
would be `self.puts`.
[top](#top)

#### Assignment
assignmenting in ruby is a little complicating.in ruby we have **lvalues** and **rvlues**. there are four kind of lvalues:

  - Variables
  - Constants
  - Attributes
  - Arrays

and four kind of _Variables_:
  - Local Variables
  - Global Variables
  - Instance Variables
  - Class Variables
  
it possible to parralel assigning:
```rb
  a=1
  q,a,z=1,2,3
  a=s=3
```
when you call _an Experssion_, **Ruby Interpretor** look for it as a variable and if could not find it, look for it in **self** object.consider below sample:
```rb
  def q
    puts 12
  end                                   #==> we define q method that print 12
  def a
    puts q                              #==> it would print 12 with q as method
  end
  q=10 if false                         #==> here Ruby find q as variable woth nil value because if clause is false.
  puts q                                #==> print nil
  q=8
  puts q
```
[top](#top)
_Attributes_ and _Array_ assigning are _lvalue_ and _rvalue_ assignment also:
```rb
  O.a = value   ====> o.a=(value)
  A[q] = w      ====> A.[]=(q,w)
  A[q,w] = e      ====> A.[]=(q,w,e)
```
also for abbreviated assignments:
```rb
  q += 2    ====> q = q + 2
  O.a += 4  ====> O.a=(O.a()+4)
  A[q] += 8 ====> A.[]=(q,A[](q)+8)
```

#### Multiple Assigning
all **Operationn** listed below:

Operator(s)|Arity|Associativety|isMethod|Operation
---|---|---|---|---
\! \~ \+|1|R|Y|Boolean NOT, bitwise complement, unary plusa
\*\*|2|R|Y|Exponentiation
\-|1|R|Y|Unary minus (define with -@)
\* \/ \%|2|L|Y|Multiplication, division, modulo (remainder)
\+ \-|2|L|Y|Addition (or concatenation), subtraction
\<\< \>\>|2|L|Y|Bitwise shift-left (or append), bitwise shift-right
\&|2|L|Y|Bitwise AND
\| \^|2|L|Y|Bitwise OR, bitwise XOR
\< \<\= \>\= \>|2|L|Y|Ordering
\=\= \=\=\= \!\= \=\~ \!\~ \<\=\>|2|N|Y|Equality, pattern matching, comparisonb
\&\&|2|L|N|Boolean AND
\|\||2|L|N|Boolean OR
.. ...|2|N|N|Range creation and Boolean flip-flops
\?\:|3|R|N|Conditional
rescue|2|L|N|Exception-handling modifier
\= \*\*\= \*\= \/\= \%\= \+\= \-\= \<\<\= \>\>\= \&\&\= \&\= \|\|\= \|\= \^\=|2|R|N|Assignment
defined?|1|N|N|Test variable definition and type
not|1|R|N|Boolean NOT (low precedence)
and or|2|L|N|Boolean AND, Boolean OR (low precedence)
if unless while until|2|N|N|Conditional and loop modifiers

there are number of differnet state:
1. same number of _lavalues_ and _rvalues_:
```rb
  q,a,z=1,2,3       #==> q=1 a=2 z=3
  q,a=a,q           #==> it would Swap values of these two variable.Parallel not Sequence
```
2. one _lavalue_ more _rvalue_:
```rb
  q = 1,2,3         #==> return q as Array q=[1,2,3]
  q, = 1,2,3        #==> return q=1
  q, = [1,2,3]      #==> return q=1
```
3. Multiple _lvalues_ and **Array** as _rvalue_:
```rb
  q,w,e = [1,2,3]   #==> Same as q,w,e=1,2,3
```
4. differences _lvalues_ and _rvalues_:
```rb
  q,w,e = 1,2       #==> q=1 w=2 z=nil
  q,w = 1,2,3       #==> q=1 w=2
```

  4/1. in this case there is an operator name **_Splat_** shown with _asterisk_ and operate like below examples:
  ```rb
    q,*w = 1,2,3          #==> q=1 w=[2,3]
    q,*w = 1,2            #==> q=1 w=[2]
    q,*w = 1,             #==> q=1 w=[]
    *q,w = 1,2,3          #==> q=[1,2] w=3
    *q,w = 1,2            #==> q=[1] w=2
    *q,w = 1,             #==> q=[] w=1
    q,w,e = 1,*[2,3]      #==> q=1 w=2 e=3
    q,w,e = 1,[2,3]       #==> q=1 w=[2,3] e=nil
    q,w,*e = 1,*[2,3,4]   #==> q=1 w=2 z=[3,4]
  ```

[top](#top)

5. Parentheses:
```rb
  q,a,z=1,[2,3]                         #==> q=1 a=[2,3] z=nil
  q,(a,z)=1,[2,3]                       #==> q=1 a=2 z=3
  q,a,z,w,s=1,[2,[3,[4,5]]]             #==> q=1 a=[2,[3,[4,5]]]
  q,(a,(z,(w,s)))=1,[2,[3,[4,5]]]       #==> q=1 a=2, z=3 w=4 s=5
```

  5/1. parallel assign in only worked with **two Parentheses** because _one parentheses_ intepreter as _method invokation_ and _no parentheses_ appeare
       _comma_ as _parameter delimeter_.
  ```rb
    puts q,a=1,2              #==> would pass q , a=1 and 2 as input parameters
    puts (q,a=1,2)            #==> like above because Parentheses is optional in method invoking
    puts ((q,a=1,2)           #==> it works as you would
  ```

6. Boolean Flip-Flop: using **Range** in _boolean expression_:
```rb
  (0..10).each {|q| print q if !(q==3..q==6)}
```
  
  6/1. in **double Dots** _left and right hand_ of range check each time but in **triple Dots** first time only evaluate _left hand_.
  ```rb
    (0..10).each {|q| print q if q==3..q>=3}        #==> print 3
    (0..10).each {|q| print q if q==3...q>=3}       #==> print 34
  ```
  6/2.it more useful and meaning for text files: below example prints each line starts with _TODO_ until Blank line:
  ```rb
    lines.each { |line| print line if line=~/^TODO/..line=~/^$/}
  ```
7. **\?\:**: in queue of these, the evaluation start fron right:

```rb
  q>a?q>z?'q':'z1':a>q?a>z?'a2':'z2':'z3'                   #==> Exception because consider a? as method
  q>a ? q>z ? 'q':'z1': a>q ? a>z ? 'a2':'z2':'z3'          #==> return z2
  q>a ? (q>z ? 'q':'z1'): (a>q ? (a>z ? 'a2':'z2'):'z3')    #==> above with parentheses
```
8. 





[top](#top)
# CodeBlocks,Functions,Procedures,Lambdas
### Code Blocks
- definition:

```rb
  {puts "Hello"}
  do
    puts "Hello"
  end
  1.upto(10).each {|n| puts n*19}
```
- we could use _Lambda_ to store _code block_ and then call it:

```rb
  lAMBDA= lambda {|q| q.each {|q| puts q}}
  lAMBDA.call(1.upto(10))
```

[top](#top)
### Functions
- definition

```rb

```
- Block as Input parameter:

```rb
  def func1(a,&codeBlock)
    a.each{|a| codeBlock.call(a)} 
  end
  func1(%w{1,2,3,4,5}) {|a| put a}  # as you see its very fun lang, we use _a_ in all block without confusing :-)
```

[top](#top)
### Procedures
- definition

```rb
  Proc.new { puts "Hello"}
  Proc.new do
    puts "Hello"
  end
  proc {puts "Hello"}
  test= proc {puts "Hello"}
  test.call()
```

[top](#top)
### Lambdas
```rb
  lambda {puts "Hello"}
  lambda do
    puts "Hello"
  end
  -> {puts "Hello"}
  -> (arg) {puts arg}
  -> { |arg| puts arg}
  test= ->{1.upto(10).each{|n| n}
  test.yield[0]
```

[top](#top)
# Class
## definition:
```rb
  class ClassName
    def initialize(InputParams)
      @InstanceVariables= assing InputParams
      ...
    end
    if defined?(@@ClassVariable)
      Some thing with @@ClassVariable
    def InstanceMethod(InputParams)
      ...
    end
    def [self|ClassName].ClassMethod(...)
      ...
    end
    private
      def ...
        ...
      end
    
    ...
  end
```
  - you could define all things in class as public and after that, use `private :def_Name :... ` to make them as private.
  - `protect :... ` is last scope of __OOP__ .
  
- What is diffrent between Instance and class variable|Method?
  - Instance Variable is define for each new Object of class but
  - Class Variable is static object in class and is shared between all Objects those creat from class
  

[top](#top)
## Inheritance:
```rb
  class ParentName
    def initialize(..)
      ...
    end
    def Test(InputParams)
      ...
    end
    ...
  end
  class ChildName < ParentName
    ...
    def Test
     ... + super   #nice!! you are free of howmany InputParams in ParentClass
  end
```
## Nested Class
Sometimes we need classes but when a main class was defined. for example we need _Pen_ , _Eraser_ , _Line_ when we want 
  _Draw_ and create _Draw_ class:
```rb
  class Draw
    def cREATE_a_Line
      Line.new
    end
    class Line
      def lINE_methode
        p "Hello I'm a line"
      end
    end
  end
```
How to call nesteded class:
```rb
  a=Draw.new
  a.lIne.lINE_methode
  b=Draw::Line.new
  b.lINE_methode
```
- actually we could access all public _class's_ methodes with double _colon_.

[top](#top)
### Class Example
```rb
  class Sequence
    include Enumerable
    def initialize(from, to, by)
      @from, @to, @by = from, to, by
    end
    def each
      x = @from
      while x <= @to
        yield x
        x += @by
      end
    end
    def length
      return 0 if @from > @to
      Integer((@to-@from)/@by) + 1
    end
    alias size length # size is now a synonym for length
    def[](index)
      return nil if index < 0
      v = @from + index*@by
      if v <= @to
        v
      else
        nil
      end
    end
    def *(factor)
      Sequence.new(@from*factor, @to*factor, @by*factor)
    end
    def +(offset)
      Sequence.new(@from+offset, @to+offset, @by)
    end
  end
```
Here is some code that uses this Sequence class:
---
>s = Sequence.new(1, 10, 2)  #--> From 1 to 10 by 2's 
>
>s.each {|x| print x } #Prints "13579"
>
>print s[s.size-1] #Prints 9
>
>t = (s+1)*2 #From 4 to 22 by 4's
---
[top](#top)
## Object
every thing in **Ruby** is _Object_ and each _Object_ is a _instanceof_ a **class**.to find the class and superclass of object:
```rb
  puts "Strings".class                                      # ==> String
  puts "Strings".class.superclass                           # ==> Object
  puts "Strings".class.superclass.superclass                # ==> BasicObject
  puts "Strings".class.superclass.superclass.superclass     # ==> nil
```
to check its class:
```rb
  puts 1234.class == Fixnum                                 # ==> true
  puts 1234.instance_of? Fixnum                             # ==> true
  puts 1234.instance_of? Numeric                            # ==> false
```
as you seen in last above example,_instance_of?_ not worked for **SubClass**.there are two other operator **is_?** and **kind_of?** which synonim of each othere.
```rb
  puts 1234.class === Numeric                               # ==> true
  puts 1234.is_a? Fixnum                                    # ==> true
  puts 1234.is_a? Numeric                                   # ==> true
  puts 1234.is_a? Integer                                   # ==> true
  puts 1234.is_a? Comparable                                # ==> true
```
there are two equality operand: **==** \~= **equal?**, **===** \~= **eql?**. these have different behavior in different classes.

and **\~=** operator defined in _String_ and _Regex_ classes.
[top](#top)
#### Object Ordering
_Ruby_ defines **<=>** operator to find _Object Ordering_. **-1** means left hand is _less than_, **0** means equality and **1** ... .
```rb
  5 <=> -5          #==> -1
  5.between? (1..10)
```
carefully consider below examples about **NaN** is a special Object
```rb
  Nan=0/0.0                 #==> it is a Not a Number
  Nan<0                     #==> false
  Nan>0                     #==> false
  Nan==0                    #==> false
  Nan==Nan                  #==> false
  Nan.equal? Nan            #==> true
```
#### Conversions
The Kernel module defines four conversion methods that behave as global conversion functions. 
These functions—**Array, Float, Integer, and String**—have the same names as the classes that they convert to, 
and they are unusual in that they begin with a capital letter.

The **Array** function attempts to convert its argument to an array by calling _to_ary_.
If that method is not defined or returns nil, it tries the to_a method.
If _to_a_ is not defined or returns nil, the **Array** function simply returns a new array containing the argument as its single element.

The **Float** function converts _Numeric_ arguments to _Float_ objects directly. For any non-Numeric value, it calls the _to_f_ method.

The **Integer** function converts its argument to a _Fixnum_ or _Bignum_. If the argument is a _Numeric_ value, it is converted directly.
_Floating-point_ values are _truncated_ rather than rounded.
If the argument is a _String_, it looks for a radix indicator(_a leading 0 for octal,0x for hexadecimal, or 0b for binary_)and converts the string accordingly.
Unlike String, _to_i_ it does not allow nonnumeric trailing characters.
For any other kind of argument, the Integer function first attempts conversion with to_int and then with _to_i_.
Finally, the String function converts its argument to a string simply by calling its to_s method.

[top](#top)
##### Coercion
Numeric types define a conversion method named coerce.The coerce method always returns an array that holds two numeric values of the same type.
The first element of the array is the converted value of the argument to coerce.
The second element of the returned array is the value (converted, if necessary) on which coerce was invoked.
```rb
  require "rational"
  r=Rational(2,3)
  r.coerce(4)               #==> [Rational(4,1),Rational(2,3)]
```
##### Boolean Conversion
**_Ruby_** is very strict in _Boolean_ class and there is not any **Boolean** Conversion except _to_s_ conversion.
in _Ruby_ only **false** and **nil** treated as **_False_** value and others would be **_Treu_** like _0_ integer and \"\" string.

#### Copying Object
the _clone_ and _dup_ method used to copy object but they are a _shallow_ to copy.if you implement **initialize_copy** method, you have different depth.

#### Marshaling Objects
_Marsha.dump_ and _Marshal.load_ are ways to **dump** and **Loading** files or any objects but there is not any guarantee between different versions.
anyway, the **YAML** is more standard way to do it.

#### Object Frozen
if you call **freeze** method on object, it would change to immutate state and never would be change. you could check this by _frozen?_ operand.
however, if you copy a frozen _object_ with **clone** it still would be _Frozen_ but if you copy with _dump_ would not.
```rb
  s= "Freezr"
  s.freeze
  s.frozen?         #==> true
  s.upcase          #==> FREEZE
  s.upcase!          #==> TypeError
```
[top](#top)

#### Objects Safe Levels
there is a **$SAFE** Levels for each Object _between 0 and 4_ levels.it useful when you work with _External Data_ to prevent make harmful those data.
below table explain these levels in short:

\$SAFE|Constraints
---|---
0|No checking of the use of externally supplied (tainted) data is performed. This is Ruby's default mode.
1|Ruby disallows the use of tainted data by potentially dangerous operations.
2|Ruby prohibits the loading of program files from globally writable locations.
3|All newly created objects are considered tainted.
4|Ruby effectively partitions the running program in two. Nontainted objects may not be modified. Typically, this will be used to create a sandbox: the program sets up an environment using a lower$SAFE level, then resets $SAFE to 4 to prevent subsequent changes to that environment.

and in more complete explain:
1. \$SAFE >= 1
    - The environment variables RUBYLIB and RUBYOPT are not processed, and the current directory is not added to the path.
    - The command-line options -e, -i, -I, -r, -s, -S, and -x are not allowed.
    - Can't start processes from $PATH if any directory in it is world-writable.
    - Can't manipulate or chroot to a directory whose name is a tainted string.
    - Can't glob tainted strings.
    - Can't eval tainted strings.
    - Can't load or require a file whose name is a tainted string.
    - Can't manipulate or query the status of a file or pipe whose name is a tainted string.
    - Can't execute a system command or exec a program from a tainted string.
    - Can't pass trap a tainted string.
2. \$SAFE >= 2
    - Can't change, make, or remove directories, or use chroot.
    - Can't load a file from a world-writable directory.
    - Can't load a file from a tainted filename starting with ~.
    - Can't use File#chmod, File#chown, File#lstat, File.stat, File#truncate, File.umask, File#flock, IO#ioctl, IO#stat, Kernel#fork, Kernel#syscall, Kernel#trap. Process::setpgid, Process::setsid, Process::setpriority, or Process::egid=.
    - Can't handle signals using trap.

[top](#top)

3. \$SAFE >= 3
    - All objects are created tainted.
    - Can't untaint objects.
4. \$SAFE >= 4
    - Can't modify a nontainted array, hash, or string.
    - Can't modify a global variable.
    - Can't access instance variables of nontainted objects.
    - Can't change an environment variable.
    - Can't close or reopen nontainted files.
    - Can't freeze nontainted objects.
    - Can't change visibility of methods (private/public/protected).
    - Can't make an alias in a nontainted class or module.
    - Can't get meta information (such as method or variable lists).
    - Can't define, redefine, remove, or undef a method in a nontainted class or module.
    - Can't modify Object.
    - Can't remove instance variables or constants from nontainted objects.
    - Can't manipulate threads, terminate a thread other than the current, or set abort_on_exception.
    - Can't have thread local variables.
    - Can't raise an exception in a thread with a lower $SAFE value.
    - Can't move threads between ThreadGroups.
    - Can't invoke exit, exit!, or abort.
    - Can load only wrapped files, and can't include modules in nontainted classes and modules.
    - Can't convert symbol identifiers to object references.
    - Can't write to files or pipes.
    - Can't use autoload.
    - Can't taint objects.

[top](#top)

<p style="color:blue">only external inout would be and could be an <b>taint</b> <I>Object</I></p>

```rb
  s="internal data"
  s.taint
  puts s.tainted?           #==> False
  s=ENV["HOME"]
  puts s.tainted?           #==> True
  s.untant                  # used only for external source
  puts s.tainted?           #==> False
```

# Namespaces, Modules and Mix-Ins
## Namespaces, Modules
simply you call other _ruby_ files to include their codes in current _ruby file_.
assume we have __File1.rb__ as below:
```rb
  module NAMESPACE1
    class ClassTest
      def classtest
        p "Hello From First NameSpace from ClassTest"
      end
    end
    def self.test
      p "Hello From First NameSpace in Module"
    end  
  end
  def test 
    p "Hello From First NameSpace"
  end
```
and __File2.rb__ like this:
```rb
  module NAMESPACE2
    class ClassTest
      def classtest
        p "Hello From First NameSpace from ClassTest"
      end
    end
    def self.test
      p "Hello From Second NameSpace in Module"
    end  
  end
  def test 
    p "Hello From Second NameSpace"
  end
```
now, in __File3.rb__ we have:
```rb
  require './File1.rb'
  require './File2.rb'
  test    <= this ambigous function calling
  NAMESPACE1.test
  NAMESPACE2.test
  NAMESPACE1::ClassTest.new.classtest
  NAMESPACE2::ClassTest.new.classtest
```
then NameSpace and Module should be clear.
## Mix-Ins
___ruby___ has not got multi inheritances.
OK! but it has solution called: __Mix-Ins__.
consider below class:
```rb
  class Test
    include Enumerable
    Const= %w {M o h a m a d}
    def each                     <= if we donot include Enumerable, we could not define this
      Const.each {|a| yield a*2}
    end
  end
```
now, we could use each item that has _iterate_ like _collect_,_select_,_detect_,_sort_ for *_Class Instance_*:
```rb
  a=Test.new
  a.each{|w| p w}
```

## Modules


[top](#top)
# Useful Class,Module,Objects,etc
## Structure
there are two useful class let you create objects fastly.
#### Struct
```rb
  ObjStruc=Structure.new(:Var1,:Var2,:Var3,...)
  obj1=ObjStruct.new(Val1,Val2,Val3,...)
```
as you see, simply you create _obj_ Object with ___Struct___ Class.
#### OPenStruct
___OpenStruct___ is the same above but you could assign _Variable_ __on the fly__.
```rb
  require 'ostruct'
  obj=OpenStruct.new
  obj.var1=val1
  obj.var2=val2
  ...
```
##### Run OS Command
the **Kernel.`** method used to run _OS Command_ in three ways:
1. _directly_: `Kernel.\`('ls')`
2. _BackQoute_: `\`ls\``
3. _%x_ literal: `%x[ls]`

[top](#top)

# Error Handeling,Debugging,Testing
an **Exception** is an _Object_ that represents somethings  go wrong. 
the Exception class defines two methods to return details about _exception_:
  1. humman readable. 
  2. end user readable.

another one is **_backtrace_** method that return _developer_ readable message like:
```rb
  filename: lineNumber in methodName
```
the list of this object and it's sublacesses are:
```rb
    Object
        +--Exception
            +--NoMemoryError
            +--ScriptError
            |
             +--LoadError
            |
             +--NotImplementedError
            |
             +--SyntaxError
            +--SecurityError                # Was a StandardError in 1.8
            +--SignalException
            |
             +--Interrupt
            +--SystemExit
            +--SystemStackError             # Was a StandardError in 1.8
            +--StandardError
            +--ArgumentError
            +--FiberError                   # New in 1.9
            +--IOError
            |
             +--EOFError
            +--IndexError
            |
             +--KeyError                    # New in 1.9
            |
             +--StopIteration               # New in 1.9
            +--LocalJumpError
            +--NameError
            |
             +--NoMethodError
            +--RangeError
            |
             +--FloatDomainError
            +--RegexpError
            +--RuntimeError
            +--SystemCallError
            +--ThreadError
            +--TypeError
            +--ZeroDivisionError
```
[top](#top)

## Error Handling
there are some kind of type to handle 
### Raise Exception
there are four kind of **Raising** Error:
1. _called_ **with no Arguments**: it creates new **RunTimeError** Object _with no message_.
2. _called_ **with single Exception Object**.
3. _called_ **with single String Message**: it creates **RunTimeError** object with this _Message_.
4. _called_ **with Exception Argument and String as message**

```rb
  raise if n < 1
  raise "Error Explanation" if n < 1
  raise RunTimeError if n < 1
  raise RunTimeError.new("Error Explanation") if n < 1
  raise RunTimeError.exception("Error Explanation") if n < 1
  raise RunTimeError,"Error: n is #{n} that should be more than one" if n < 1
```
5. we could use existing _Exception clss_:
```rb
  a=0
  if a==0
    raise ArgumentError, "entrance should not be 0"
  else
    puts 10/a
  end
```
6. or create an Execption Class
```rb
  class MySelfExcept < RuntimeError
    ...
  end
  a=0
  if a==0
    raise MySelfExcept, "a should not be 0"
  else
    puts 10/a
  end
```

[top](#top)
### Rescue Exception
**Rescue** is useful to handle _Error_ that we have not anticipated it.it has below structure
```rb
  begin
    ... # program code goes here and if there is not any error it continues after end
  rescue
    ... # if any Exception occures in above code that we did not anticipate it, code jump here.
  else
    ...
  ensure
    ...
  
```
1. if you didn't define **Runtime Exception** in **_Rescue_** , the occured exception would propagate itself to out block until find _runtime_ exception or quit program.
2. if a new _Exception_ occured during handling the exception that raise by _rescue_, this exception omit ,new exception raise and nothing can handle it.
3. **retry** used in _rescue_ to retry the block.
```rb
  require 'open-uri'
  tries=0
  begin
    tries++
    open('http://www.example.com')
  rescue OpenURI::HTTPERROR => e
    puts e.message
    if tries<4
      retry
    end
  end
```
4. the **else** statement execute whenever there are no _Exception_ and **rescue** wouldn't _trigger_.
5. **No _rescue_ mode uses after _else_ in this block**.
6. **ensure** clause _execute_ anyway.
  1. if  no _exception_, after else if it exist, _ensure_ cluase would be run.
  2. if there is a **return** _statement_, _Execution_ skip _else_ and run _ensure_ clause.
  3. 

[top](#top)
##### Naming Error in Rescue
1. **\$\!** is _global variable_ that refers to this _Statement Error_.
2. if we use **require 'English'** in code, we could use **$ERROR_INFO** variable instead **\$\!**.
3. last one of naming is below:
```rb
  begin
    puts 10 / 0
  rescue ExceptioClassName=> e
    puts "the #{e.class}: #{e.message}".
  rescue => e                                   #=>Nothing used means StandardError Exception Class
    puts "the #{e.class}: #{e.message}".
  rescue {ArgumentError|TypeError}
    puts $!
  end
```

[top](#top)
##### if you rather continue your program after exception, use this.
```rb
  begin
    your Codes
  rescue {non Exception Class Name|Your Self Exception Class Name|Exception|Nothing}
    message and code to continue the app.
  end
```
  * the result woud ne : ZeroDivisionError

### Catch Throw
how you break out from a loop? _by *break* command_<br/>
how you break out from neasted loop completely? _by *Catch Throw* command_
```rb
  fileName.each do |fileName|
    catch :missing_data
    fileName.each do |line|
      line.each do |col|
        throw :missing_data
        # if not error, do what you want
      end
    end
  end
```
- it is not necessary to use _catch_ and _throw_ in the same block
- 

[top](#top)
## Debugging

[top](#top)
## Testing

[top](#top)
# Documentation
### Embeded Documentation
this type of Doc strats with _=begin_ and continue until find _=end_.
```rb
  ...
  # =start Any thing is comment
  any code blocks are ignored
  # =end this is the end of embeded
```

### Documentation Comments
Ruby is powerfull,I emphasize: it is powerfull.
**rdoc** is _one of **Ruby Documentation** Packages_. it has sample markup language:
```rb
  # Blank Line is seperate line
  #
  # = is make headline and == make is a subheading and === ... <- Without indent
  #  indented make a verbatim 
  # Ordered List by No.
  # unOrdered List by * or - 
  # _,*,+ for Italic,Bold,Code recursively.
  # There is no way to nested List.
  # [Item] the description for Item
```
and example the doc for:
```rb
  #= Create Person Class To store his Details
  class Person
    attr_accessor :name,:family,:idNumber,:job
    #== create Initializer function
    #* what are items should you provide
    # * *Person* _Identify_ _number_
    # * *Person* _name_ _and_ _family_
    # * *Person* _Job_
    def initialize(name,family,idNumber,job)
      @name=name
      @family=family
      @idNumber=idNumber
      @job=job
    end
    #== print Person Details
    # def print
    #   puts "Mr/Ms #{@name}  #{@family} works in #{@job} and his/her Identifer Number is #{@idNumber}"
    # end
    def print
      puts "Mr/Ms #{@name}  #{@family} works in #{@job} and his/her Identifer Number is #{@idNumber}"
    end
    # prevent to generate Doc for this func
      def hiddenFunc #:nodoc:
        put "this wouldn't in documentation"
      end
    # I would like to make comment for my self how?
    # 
    # let turn it off _with_ _2_ _minus_
    # 
    #--
    # two minus will it off and two plus turn it on P-)
    #++
    # now,we turn doc on _with_ _2_ _Plus_
    def otherDef
      put "nothing"
    end
  end
```
will be this page,[Please Download this Page and open in Browser to find the result](#Images/rdoc1.html)
[top](#top)

[top](#top)

[top](#top)

[top](#top)

[top](#top)
